<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{$post->title}}
        </h2>
    </x-slot>

<!-- component -->
    <div class="max-w-4xl px-10 py-6 mx-auto bg-white rounded-lg shadow-md">

    <img src="{{asset('/storage/'.$post->image) }}" alt="">
           
    <div>
        {{$post->content}}
    </div>
    </div>
</x-app-layout>
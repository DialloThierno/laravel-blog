<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Prophecy\Doubler\Generator\Node\ReturnTypeNode;

class DashboardController extends Controller
{
    //
    public function index(){
        
        $posts = auth()->user()->posts;

        return view('dashboard',compact('posts'));
    }
}
